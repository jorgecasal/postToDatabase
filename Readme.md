# POST TO DATABASE

postToDatabase is a MERN application that can be adapted to your needs. Flexibility its postToDatabase's biggest advantage.
Simply a database that can be used as a bug tracker, a personal agenda, a todo list, or even a Kanye West quote data base.

## up and running with:

npm install && npm run client-install && npm run dev

## Usage

Add an item to see it displayed on screen while being autmatically updated in the database. 

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
